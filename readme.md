<img src="https://pub-00bc7a7c079d493391109fe14bbac1f3.r2.dev/destruct%20-%20desktop.webp" alt="destruct - desktop" width="100%">

# DESTRUCT DESKTOP
- DESTRUCT DESKTOP is a desktop client for the @destruct audio library
- It's currently in development, and will sync the library to your local machine
- Binaries for Linux, Mac, and Windows will be freely available when it's stable
- The source code found in the src directory is [MIT](https://gitlab.com/destruct/destruct-desktop/-/blob/main/license.md) licensed

## KEYMAP
<img src="https://pub-00bc7a7c079d493391109fe14bbac1f3.r2.dev/destruct%20-%20keymap.webp" alt="keymap" width="100%">
<table style="width: 100%;">
  <tr><td>[ D ] - Download</td><td>[ X ] - Clear Search</td><td>[ ⭡ ] - Play Previous</td></tr>
  <tr><td>[ R ] - Randomize</td><td>[ ESC ] - Escape Search</td><td>[ ⭣ ] - Play Next</td></tr>
  <tr><td>[ L ] - Toggle Loop</td><td>[ ENTER ] - Enter Search</td><td>[ ⭠ ] - Scrub Backward</td></tr>
  <tr><td>[ S ] - Focus Search</td><td>[ SPACE BAR ] - Play / Pause</td><td>[ ⭢ ] - Scrub Forwards</td></tr>
</table>
