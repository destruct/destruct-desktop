use anyhow::Result;
use std::ffi::OsString;
use cocoa::appkit::NSPasteboard;
use cocoa::foundation::{NSString, NSURL};
use cocoa::{base::nil, foundation::NSArray};

pub fn copy_files(path: &str){
    let entry: OsString = path.into();
    let clip = Clip::new(entry).unwrap();
    clip.copy_files().unwrap();
}

trait Clipboard: Sized {
    fn new(entry: OsString) -> Result<Self>;
    fn copy_files(&self) -> Result<()>;
}

struct OSXClipboard {
    entry: OsString,
}

type Clip = OSXClipboard;

impl Clipboard for OSXClipboard {
    fn new(entry: OsString) -> Result<OSXClipboard> {
        Ok(OSXClipboard { entry })
    }
    fn copy_files(&self) -> Result<()> {
        let mut ns_urls = vec![];
        let paste_board = unsafe { NSPasteboard::generalPasteboard(nil) };
        let entry_str = self.entry.clone().into_string().unwrap();
        let path = unsafe { NSString::alloc(nil).init_str(&entry_str) };
        let ns_url = unsafe { NSURL::alloc(nil).initFileURLWithPath_(path) };
        ns_urls.push(ns_url);
        unsafe {
            let objs = NSArray::arrayWithObjects(nil, &ns_urls);
            paste_board.clearContents();
            paste_board.writeObjects(objs);
        }
        Ok(())
    }
}
