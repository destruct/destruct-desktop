#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use tauri::{Manager, Window, AppHandle, Emitter};
use tracing_subscriber::layer::SubscriberExt;
use window_vibrancy::{apply_vibrancy, NSVisualEffectMaterial};

mod sync;
#[tauri::command]
async fn sync(){
    println!("wtf");
    sync::sync_files().await;
}

mod copy;
#[tauri::command]
fn copy(path: String){
    copy::copy_files(path.as_str())
}

#[tauri::command]
async fn close_splashscreen(window: Window){
    window.get_webview_window("splashscreen").expect("no window labeled 'splashscreen' found").close().unwrap();
    window.get_webview_window("main").expect("no window labeled 'main' found").show().unwrap();
}

struct WebviewConsole(AppHandle);

impl std::io::Write for WebviewConsole {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let app = self.0.clone();
        let payload = String::from_utf8_lossy(buf);
        let len = payload.len();
        println!("{}", payload);
        if payload.contains("sync worker has been completed"){
            app.emit("finish-event", "").unwrap();
        };
        if payload.contains("sync completed"){
            let split1: Vec<&str>  = payload.split(r#""key":""#).collect();
            let split2: Vec<&str>  = split1[1].split(r#"","real_path""#).collect();
            let split3: Vec<&str>  = split2[0].split("/").collect();
            let name = split3[2];
            println!("{:?}", name);
            app.emit("test-event", name).unwrap();
        };
        Ok(len)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

fn main() {
    tauri::Builder::default()
        .plugin(tauri_plugin_fs::init())
        .plugin(tauri_plugin_shell::init())
        .plugin(tauri_plugin_store::Builder::new().build())
        .setup(|app| {
            let window = app.get_webview_window("main").unwrap();
            let splash = app.get_webview_window("splashscreen").unwrap();

            #[cfg(target_os = "macos")]
            apply_vibrancy(&window, NSVisualEffectMaterial::HudWindow, None, Some(10.0)).unwrap();
            apply_vibrancy(&splash, NSVisualEffectMaterial::HudWindow, None, Some(10.0)).unwrap();

            let app = app.app_handle().clone();
            let writer = std::sync::Mutex::new(WebviewConsole(app));
            let webview = tracing_subscriber::fmt::Layer::default()
                .with_line_number(false)
                .with_ansi(false)
                .without_time()
                .with_target(false)
                .with_level(false)
                .json()
                .flatten_event(true)
                .with_writer(writer);
            let subscriber = tracing_subscriber::Registry::default().with(webview);
            tracing::subscriber::set_global_default(subscriber).unwrap();

            Ok(())
         })
        .invoke_handler(tauri::generate_handler![copy, sync, close_splashscreen])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
