class CompGrid extends HTMLElement{
    connectedCallback() {
        const sr = this.attachShadow({ mode: "closed" })
        sr.innerHTML = `<canvas id="canvas" style="width: 100%; height: 100%;"></canvas>`;

        /*---Initialize---*/
        const canvas = sr.getElementById("canvas");
        const ctx = canvas.getContext("2d");
        let gridSize = 12;
        let gridArray = [];

        function config(){
            gridArray = []
            canvas.width = canvas.offsetWidth * dpr;
            canvas.height = canvas.offsetHeight * dpr;
            ctx.lineCap = "round";
            ctx.lineWidth = 4;
            let xOffset = (canvas.width % gridSize) / 2;
            if (xOffset == 0) { xOffset = gridSize }
            let yOffset = (canvas.height % gridSize) / 2;
            if (yOffset == 0) { yOffset = gridSize }
            for (let y = yOffset; y <= canvas.height; y += gridSize) {
                for (let x = xOffset; x <= canvas.width; x += gridSize) {
                    gridArray.push([x, y]);
                }
            }
        };
        config();

        /*---Draw---*/
        function drawDot(xy) {
            ctx.beginPath();
            ctx.moveTo(xy[0], xy[1]);
            ctx.lineTo(xy[0], xy[1] + 0.1);
            ctx.stroke();
        };

        function gradient() {
            const gradient = ctx.createConicGradient((performance.now() * 0.0004), 70, canvas.height + 80);
            gradient.addColorStop(0, "#222");
            gradient.addColorStop(0.1, colorActive);
            gradient.addColorStop(0.2, "#222");
            gradient.addColorStop(0.3, colorActive);
            gradient.addColorStop(0.4, "#222");
            gradient.addColorStop(0.5, "#222");
            gradient.addColorStop(0.6, colorActive);
            gradient.addColorStop(0.7, "#222");
            gradient.addColorStop(0.85, "#222");
            gradient.addColorStop(0.9, colorActive);
            gradient.addColorStop(1, "#222");
            ctx.strokeStyle = gradient;
        }

        function draw() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            gradient();
            for (let i = 0; i < gridArray.length; i++) {
                drawDot(gridArray[i]);
            };
        };

        /*---Animate---*/
        function animate(){
            draw()
            requestAnimationFrame(animate)
        }
        animate();

        /*---Events---*/
        this.addEventListener("resize", config);
    }
}
window.customElements.define("comp-grid", CompGrid);
