class CompSplashLoader extends HTMLElement{
    connectedCallback(){
        const sr = this.attachShadow({mode: "open"});
        sr.innerHTML = `
            <style>
                #wrapper{
                    width: 200px;
                    height: 200px;
                    position: relative;
                }
            </style>
            <div id="wrapper">
                <svg stroke-linecap="straight" stroke-width=".5" version="1.1" viewBox="0 0 32 32" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">


                    <defs>
                        <path id="masker" d="m7.0019 4c-1.6579-2.9e-5 -3.002 1.344-3.002 3.002v17.996c-3.61e-4 1.6581 1.3439 3.0023 3.002 3.002h8.7383l-0.50781-7.8418c-0.05454-0.84648-1.0348-1.286-1.7031-0.76367l-4.2461 3.3223 3.3223-4.2461c0.54143-0.69181 0.12004-1.7097-0.75195-1.8164l-5.3535-0.6543 5.3516-0.6543c0.87231-0.10607 1.2947-1.1238 0.75391-1.8164l-3.3223-4.2461 4.2441 3.3203c0.66962 0.52184 1.6502 0.08153 1.7051-0.76562l0.50781-7.8379zm9.2578 0 0.50781 7.8418c0.05454 0.84648 1.0348 1.286 1.7031 0.76367l4.2461-3.3223-3.3223 4.2461c-0.54143 0.69181-0.12004 1.7097 0.75195 1.8164l5.3535 0.6543-5.3516 0.6543c-0.87231 0.10607-1.2947 1.1238-0.75391 1.8164l3.3223 4.2461-4.2441-3.3203c-0.66962-0.52184-1.6502-0.08153-1.7051 0.76562l-0.50781 7.8379h8.7383c1.6579 2.9e-5 3.002-1.344 3.002-3.002v-17.996c3.61e-4 -1.6581-1.3439-3.0023-3.002-3.002z"/>

                        <path id="wire" fill="oklch(61% 0.38 14)" d="m7 4c-1.6569-1.49e-4 -3.0001 1.3431-3 3v18.002c-7.27e-4 1.6553 1.3408 2.9977 2.9961 2.998h8.7441l-0.50781-7.8418c-0.05423-0.84667-1.0349-1.2864-1.7031-0.76367l-4.2461 3.3223 3.3184-4.2422c0.54456-0.69366 0.12156-1.7161-0.75391-1.8223l-5.3477-0.65234 5.3516-0.6543c0.87227-0.1061 1.2947-1.1238 0.75391-1.8164l-3.3223-4.2461 4.2676 3.3379c0.65878 0.51414 1.6244 0.081879 1.6797-0.75195l0.50976-7.8691h-8.7402zm9.2598 0 0.50781 7.8418c0.05423 0.84667 1.0349 1.2864 1.7031 0.76367l4.2461-3.3223-3.3184 4.2422c-0.54456 0.69366-0.12156 1.7161 0.75391 1.8223l5.3477 0.65234-5.3516 0.6543c-0.87227 0.1061-1.2947 1.1238-0.75391 1.8164l3.3223 4.2461-4.2676-3.3379c-0.65878-0.51414-1.6244-0.081879-1.6797 0.75195l-0.50976 7.8691h8.7402c1.6569 1.49e-4 3.0001-1.3431 3-3v-18.002c7.27e-4 -1.6553-1.3408-2.9977-2.9961-2.998h-8.7441zm-9.2598 0.16016h8.5684l-0.49805 7.6992c-0.047399 0.71476-0.85717 1.0774-1.4219 0.63672l-5.2988-4.1465 4.1309 5.2793h-0.001953c0.46916 0.6008 0.11019 1.4666-0.64648 1.5586l-6.6465 0.8125 6.6426 0.81055c0.75971 0.09216 1.121 0.96251 0.64844 1.5645v0.001953l-4.127 5.2754 5.2773-4.1309c0.57405-0.44907 1.3987-0.08092 1.4453 0.64648l0.49609 7.6719h-8.5723c-1.5686 0-2.8359-1.2693-2.8359-2.8379v-18.002c0-1.5702 1.2696-2.8398 2.8398-2.8398zm9.4316 0h8.5723c1.5686 0 2.8359 1.2693 2.8359 2.8379v18.002c0 1.5702-1.2696 2.8398-2.8398 2.8398h-8.5684l0.49805-7.6992c0.047399-0.71476 0.85717-1.0774 1.4219-0.63672l5.2988 4.1465-4.1309-5.2793h0.001953c-0.46916-0.6008-0.11019-1.4666 0.64648-1.5586l6.6465-0.8125-6.6426-0.81055c-0.75971-0.09216-1.121-0.96251-0.64844-1.5645v-0.001953l4.127-5.2754-5.2773 4.1309c-0.57405 0.44907-1.3987 0.08092-1.4453-0.64648l-0.49609-7.6719z"/>
                    </defs>

                    <mask id="maskBot">
                        <rect fill="#fff" width="32" height="32"/>
                        <use xlink:href="#masker" class="top" fill="#000"/>
                        <use xlink:href="#masker" class="mid" fill="#000"/>
                        <use xlink:href="#masker" class="low" fill="#000"/>
                    </mask>

                    <mask id="maskLow">
                        <rect fill="#fff" width="32" height="32"/>
                        <use xlink:href="#masker" class="top" fill="#000"/>
                        <use xlink:href="#masker" class="mid" fill="#000"/>
                    </mask>

                    <mask id="maskMid">
                        <rect fill="#fff" width="32" height="32" />
                        <use xlink:href="#masker" class="top" fill="#000"/>
                    </mask>

                    <mask id="maskLoad">
                        <rect fill="#fff" width="32" height="32" />
                        <rect id="load" x="4" y="4" width="24" height="24" ry="0"/>
                    </mask>

                    <g mask="url(#maskBot)">
                        <use class="bot" xlink:href="#wire" fill-opacity="0.2"/>
                    </g>
                    <g mask="url(#maskLow)">
                        <use class="low" xlink:href="#wire" fill-opacity="0.4"/>
                    </g>
                    <g mask="url(#maskMid)">
                        <use class="mid" xlink:href="#wire" fill-opacity="0.7"/>
                    </g>
                    <g>
                        <use class="top" xlink:href="#wire" fill-opacity="1.0"/>
                        <use class="top" xlink:href="#masker" mask="url(#maskLoad)" fill="oklch(61% 0.38 14)"/>
                    </g>

                </svg>
            </div>
        `;

        let top = sr.querySelectorAll(".top");
        let mid = sr.querySelectorAll(".mid");
        let low = sr.querySelectorAll(".low");
        let bot = sr.querySelectorAll(".bot");

        function transform(x, phaseOffset, yOffset, speed){
            return `translate(0, ${0.5 * (Math.sin((x - phaseOffset) * speed)) + yOffset}px)`;
        }

        let offset = 0;
        function animate(){
            offset += 0.3;
            bot.forEach((element) => {element.style.transform = transform(offset, 0, 3, 0.07)});
            low.forEach((element) => {element.style.transform = transform(offset, 4, 1, 0.07)});
            mid.forEach((element) => {element.style.transform = transform(offset, 8, -1, 0.07)});
            top.forEach((element) => {element.style.transform = transform(offset, 12, -3, 0.07)});
            sr.getElementById("load").style.transform = `translate(0, -${(lineCount / 2608) * 23}px)`;
            requestAnimationFrame(animate);
        };
        animate();

        window.addEventListener("downloading", () => {downloading = true});
    };
};
window.customElements.define("comp-splash-loader", CompSplashLoader);
