class CompVis extends HTMLElement{
    connectedCallback(){
        const sr = this.attachShadow({mode: "open"});
        sr.innerHTML = `
            <style>
                #wrapper{
                    width: 25px;
                    height: 25px;
                    cursor: pointer;
                }
                #img{
                    width: 100%;
                    width: 100%;
                }
                @media(hover){
                    #img:hover{
                        stroke: var(--active);
                    }
                }

                #large-iris{r: 8px;}
                #small-iris{r: 3px;}

                #top-lid, #bottom-lid, #large-iris, #small-iris{
                    transition-duration: 0.2s;
                    easing: "ease-out";
                }
                #img[open="false"] #top-lid{transform: translateY(3.5px) scaleY(70%)}
                #img[open="false"] #bottom-lid{transform: translateY(3.5px) scaleY(70%)}

                #img[open="false"] #large-iris{r: 5.5px;}
                #img[open="false"] #small-iris{r: 0px;}


            </style>

            <div id="wrapper">
                <svg id="img" open="true" fill="none" stroke="#222" stroke-linecap="round" stroke-width="1.5" version="1.1" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path id="top-lid" d="m1 12s3-8 11-8 11 8 11 8"/>
                    <path id="bottom-lid"d="m23 12s-3 8-11 8-11-8-11-8"/>
                    <circle id="large-iris" cx="12" cy="12"/>
                    <circle id="small-iris" cx="12" cy="12"/>
                </svg>
            </div>
        `;

        window.addEventListener("toggleVis", () => {
            if(stateVis){sr.getElementById("img").setAttribute("open", "true")}
            else{sr.getElementById("img").setAttribute("open", "false")}
        });
    }
}
window.customElements.define("comp-vis", CompVis);
