class CompSampleGrid extends HTMLElement {
    connectedCallback(){
        const sr = this.attachShadow({mode: "closed"});
        sr.innerHTML = `
            <style>
                #main-wrapper{
                    width: 100%;
                    min-width: 360px;
                    height: 100vh;
                    overflow-y: scroll;
                    overscroll-behavior: none;
                    border: 1.5px solid #222;
                    border-radius: 0px 10px 10px 0px;
                    box-sizing: border-box;
                    position: relative;
                }
                #main-wrapper::-webkit-scrollbar{
                    display: none;
                }
                #max-width-wrapper{
                    display: flex;
                    width: 100%;
                    justify-content: center;
                    padding-top: calc(46.5px + 250px);
                }
                    #sample-grid{
                        width: min(100%, 2290px);
                        display: grid;
                        padding: 5px 10px;
                        grid-template-columns: 1fr;
                        touch-action: pan-y;
                        overflow-y: scroll;
                    }
                        .sample{
                            display: grid;
                            grid-template-columns: 130px 200px auto minmax(0, 1400px) auto 60px 120px 20px;
                            grid-template-rows: 70px;
                            grid-template-areas: "type name space1 wav space2 key bpm dl";
                            font-size: 15px;
                            align-items: center;
                            white-space: nowrap;
                            padding: 0 20px;
                            margin: 5px 0;
                            border-radius: 10px;
                            color: var(--bright);
                            transition: background-color 0.5s, color 0.5s;
                        }
                            .sample[pointer-area="true"]{
                                color: var(--active);
                                background-color: var(--main-low);
                                transition-duration: 0s;
                            }
                            @media screen and (max-width: 1280px){
                                .sample{
                                    padding: 10px 10px 0 10px;
                                    grid-template-columns: 3fr 8fr 0 0 1fr 3fr 25px;
                                    grid-template-rows: 70px 30px;
                                    gap: 0 10px;
                                    grid-template-areas:
                                        "wav wav wav wav wav wav wav"
                                        "type name space1 space2 key bpm dl";
                                }
                            }
                            .fullscale{
                                width: 100%;
                                height: 100%;
                                display: flex;
                                align-items: center;
                            }
                            .active-sample{
                                color: var(--active);
                                transition: background-color 0s, color 0s;
                                background-color: var(--main-mid);
                            }
                        .divider{
                            width: 100%;
                            background-color: #ffffff08;
                            height: 4px;
                            border-radius: 2px;
                        }
                #toolbar{
                    position: fixed;
                    top: 0;
                    left: 220.5px;
                    width: calc(100vw - 220.5px);
                    border-radius: 0 8.5px 0 0;
                    min-width: 356px;
                    height: 46.5px;
                    box-sizing: border-box;
                    padding: 0 10px;
                    border: 1.5px solid #222;
                    background-color: #0c0c0ccc;
                    backdrop-filter: blur(6px);
                    -webkit-backdrop-filter: blur(6px);
                    transform: translate3d(0, 0, 0); /* safari blur freeze fix */

                    display: grid;
                    grid-template-columns: auto 1fr min-content min-content min-content;
                    align-items: center;
                    justify-content: center;
                    gap: 10px;
                }
                #spectrum{
                    position: fixed;
                    top: 45px;
                    left: 222px;
                    width: calc(100vw - 222px);
                    height: 250px;
                    border: 1.5px solid #222;
                    background-color: #0c0c0ccc;
                    box-sizing: border-box;
                    backdrop-filter: blur(8px);
                    -webkit-backdrop-filter: blur(8px);
                    transform: translate3d(0, 0, 0); /* safari blur freeze fix */
                }
                @media(hover){
                    #sample-grid[drag="false"] .sample:hover{
                        color: var(--active);
                        background-color: var(--main-low);
                        transition-duration: 0s;
                    }
                    #sample-grid[drag="false"] .active-sample:hover{
                        background-color: var(--main-mid);
                    }
                    #sample-grid[drag="false"] #load-more:hover{
                        background-color: var(--active);
                        border-color: var(--active);
                        transition-duration: 0s;
                    }
                }
            </style>

            <div id="main-wrapper">
                <div id="max-width-wrapper">
                    <div id="sample-grid" drag="false"></div>
                </div>

                <div id="toolbar">
                    <!--comp-sync></comp-sync-->
                    <!--div style="width: 300px; height: 26px; border: #222 solid 1.5px; border-radius: 4px; color: var(--active); font-size: 15px; display: flex; align-items: center; ">search</div-->
                    <div><!--spacer--></div>
                    <div><!--spacer--></div>
                    <comp-playback id="playback"></comp-playback>
                    <comp-vis id="toggle-vis"></comp-vis>
                    <comp-icon id="web-link" img="/assets/destruct.svg" height="25px" width="25px" color="var(--active)"></comp-icon>
                </div>

                <div id="spectrum">
                    <comp-spectrum></comp-spectrum>
                    <!--comp-grid></comp-grid-->
                </div>
        `;
        /*---LINK---*/
        const { open } = window.__TAURI_PLUGIN_SHELL__;
        sr.getElementById("web-link").addEventListener("click", () => {
            open("https://destruct.dev")
        })

        /*---DB QUERY---*/
        function where(){
            if(stateSearch === "" && stateGroup === "✶" && stateType === "✶"){return ""};
            let where = "WHERE ";
            if(stateType != "✶"){where += `[type] = '${stateType}'`}
            else if(stateGroup != "✶"){where += `[group] = '${stateGroup}'`};
            if(stateSearch != ""){
                if(stateGroup != "✶" || stateType != "✶"){where += " AND "};
                where += `lower([title]) LIKE '%${stateSearch.toLowerCase()}%'`;
            };
            return where;
        };

        function orderBy(){
            switch(stateSort){
                case "az": return 'ORDER BY [title] ASC';
                case "za": return 'ORDER BY [title] DESC';
                case "new": return 'ORDER BY [date] ASC';
                case "old": return 'ORDER BY [date] DESC';
                default: return 'ORDER BY [hash] ASC';
            };
        };

        function offset(page){return `LIMIT ${pageSize} OFFSET ${page * pageSize}`};

        function query(page){
            return `SELECT * FROM samples ${where()} ${orderBy()} ${offset(page)};`;
        };

        async function dbQuery(page){
            let res = [];
            db.exec({
                sql: query(page),
                rowMode: 'object',
                callback: function(row){res.push(row);}
            });
            return res; //console.log(res);
        };

        function clearObserver(){
            observer.unobserve(sr.getElementById("observer"));
            sr.getElementById("observer").remove();
        };

        let activePage = 0;
        const observer = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if(entry.isIntersecting){
                    activePage = parseInt(entry.target.getAttribute("page"));
                    dbQuery(activePage + 1).then(res => {
                        if(res.length == pageSize){samples(res, activePage + 1, "beforeend"); return;};
                        if(res.length < pageSize && res.length > 0){samples(res, activePage + 1, "beforeend");};
                        clearObserver();
                    });
                };
            });
        });

        function samples(records, pageIndex, placement){
            if(sr.getElementById("observer") != null){clearObserver()};
            let output = `<div class="page" page="${pageIndex}"><div id="observer" page="${pageIndex}" ></div>`;
            let index = pageIndex * pageSize;

            records.forEach(function(sample){
                let divider = index == 0 ? "" : `<div class="divider"></div>`;
                output += `
                    ${divider}
                	<div index="${index}" class="sample" pointer-area="false">
                        <div style="grid-area: type;" class="play fullscale">${sample.type}</div>
                        <div style="grid-area: name; overflow-y: hidden;" class="play fullscale">${sample.name}</div>
                        <div style="grid-area: space1;" class="play fullscale"></div>
                        <comp-waveform style="grid-area: wav; height: 65px;" class="play" number="${index}" date="${sample.date}" file="${sample.group}/${sample.type}/${sample.title}.wav" waveform="${sample.waveform}"></comp-waveform>
                        <div style="grid-area: space2;" class="play fullscale"></div>
                        <div style="grid-area: key; justify-content: center; padding-left: 20px;" class="play fullscale">${sample.key}</div>
                        <div style="grid-area: bpm; justify-content: center;" class="play fullscale">${sample.bpm}</div>
                        <comp-copy style="grid-area: dl;" copyindex="${index}" file="${sample.group}/${sample.type}/${sample.title}.wav"></comp-copy>
                    </div>
                `;
                index += 1;
            });
            output += `</div>`;
            sr.getElementById("sample-grid").insertAdjacentHTML(placement, output);
            observer.observe(sr.getElementById("observer"));

            let samples = sr.querySelector(`[page="${pageIndex}"]`).querySelectorAll(".sample");
            samples.forEach(sample => {
                sample.querySelectorAll(".play").forEach(area => {
                    area.addEventListener("pointerdown", () => {
                        if(pointerArea == null){
                            pointerArea = parseInt(area.parentNode.getAttribute("index"))
                        };
                    });
                });

                sample.addEventListener("pointerover", () => {
                    if(!globalDrag){
                        sample.querySelector("comp-copy").dispatchEvent(new CustomEvent("active"));
                    }
                });

                sample.addEventListener("pointerout", () => {
                    if(sample.getAttribute("index") != activeIndex){
                        sample.querySelector("comp-copy").dispatchEvent(new CustomEvent("inactive"));
                    }
                });
            })
        };

        window.addEventListener("push-state", () => {
            activeIndex = -1;
            sr.querySelectorAll("comp-waveform").forEach(comp => {comp.dispatchEvent(new CustomEvent("stop"))});
            sr.getElementById("sample-grid").innerHTML = "";
            dbQuery(0).then(res => samples(res, 0, "afterbegin"));
        });

        /*---HANDLE PLAY---*/
        function handlePlay(index){
            if(activeIndex != index){
                if(activeIndex != -1){
                    sr.querySelector(`[index="${activeIndex}"]`).classList.remove("active-sample");
                    sr.querySelector(`[number="${activeIndex}"]`).dispatchEvent(new CustomEvent("stop"));
                    sr.querySelector(`[copyindex="${activeIndex}"]`).dispatchEvent(new CustomEvent("inactive"));
                };
                if(!globalDrag){sr.querySelector(`[number="${index}"]`).dispatchEvent(new CustomEvent("reset-progress"))};
                sr.querySelector(`[index="${index}"]`).classList.add("active-sample");
                sr.querySelector(`[copyindex="${index}"]`).dispatchEvent(new CustomEvent("active"));
                activeIndex = index;
            };
            sr.querySelector(`[number="${index}"]`).dispatchEvent(new CustomEvent("handlePlay"));
        };

        /*---SCROLL TO ACTIVE---*/
        function scrollToActive(){
            let activePos = sr.querySelector(`[index="${activeIndex}"]`).getBoundingClientRect();
            let behavior = "smooth";
            if(activePos.top < searchHeight || activePos.bottom > window.innerHeight){behavior = "instant"};
            sr.querySelector(`[index="${activeIndex}"]`).scrollIntoView({behavior: behavior, block: 'center'});
        };

        /*---DRAG---*/
        let capturePointer;
        window.addEventListener("pointerdown", e => {
            if(pointerArea != null){capturePointer = e.offsetX};
        });

        let locked = false;
        window.addEventListener("pointermove", e => {
            if(globalDrag && !locked){
                locked = true;
                sr.getElementById("sample-grid").style.touchAction = "pan-x";
                sr.getElementById("sample-grid").setAttribute("drag", "true");
                document.body.style.cursor = "col-resize";
            }
            if(pointerArea != null){
                globalPointerX = e.offsetX;
                if(Math.abs(globalPointerX - capturePointer) > 10 && !globalDrag){
                    globalDrag = true;
                    if(pointerArea != activeIndex){
                        sr.querySelector(`[number="${pointerArea}"]`).parentElement.setAttribute("pointer-area", "true");
                    };
                    sr.querySelector(`[number="${pointerArea}"]`).dispatchEvent(new CustomEvent("drag"));
                };
            };
        });

        window.addEventListener("pointerup", () => {
            if(pointerArea != null){
                handlePlay(pointerArea);
                sr.querySelector(`[number="${pointerArea}"]`).parentElement.setAttribute("pointer-area", "false");
            };
            sr.getElementById("sample-grid").style.touchAction = "pan-y";
            sr.getElementById("sample-grid").setAttribute("drag", "false");
            document.body.style.cursor = "default";
            pointerArea = null;
            globalDrag = false;
            locked = false;
        });

        window.addEventListener("pointercancel", e => {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            if(!globalDrag){
                pointerArea = null;
            };
        });

        /*---LOOP---*/
        function controlLoop(){
            if(!stateLoop){stateLoop = true}
            else{stateLoop = false};
            sr.getElementById("playback").dispatchEvent(new CustomEvent("playbackChange", {bubbles: true, composed: true}));
        };
        sr.getElementById("playback").addEventListener("pointerdown", controlLoop);

        /*---Toggle Visualizer---*/
        function toggleVis(){
            if(!stateVis){
                stateVis = true;
                sr.getElementById("spectrum").style.display = "block";
                sr.getElementById("max-width-wrapper").style.paddingTop = "calc(46.5px + 250px)";
            }
            else{
                stateVis = false;
                sr.getElementById("spectrum").style.display = "none";
                sr.getElementById("max-width-wrapper").style.paddingTop = "46.5px";
            }
            window.dispatchEvent(new CustomEvent("toggleVis", {bubbles: true, composed: true}));
        };
        sr.getElementById("toggle-vis").addEventListener("pointerdown", toggleVis);

        /*---Toggle Sidebar---*/
        window.addEventListener("toggleSidebar", () => {
            if(stateSidebar){
                sr.getElementById("main-wrapper").style.borderRadius = "0px 10px 10px 0px";
                sr.getElementById("spectrum").style.left = "222px";
                sr.getElementById("spectrum").style.width = "calc(100vw - 222px)";
            }
            else{
                sr.getElementById("main-wrapper").style.borderRadius = "10px";
                sr.getElementById("spectrum").style.left = "0";
                sr.getElementById("spectrum").style.width = "100vw";
            };
        });

        /*---KEYBOARD HANDLING---*/
        window.addEventListener("keydown", e => {
            if(!stateFocus){
                switch(true){
                    case (e.key == " "): //play pause
                        e.preventDefault();
                        if(activeIndex == -1){activeIndex = 0};
                        handlePlay(activeIndex);
                        break;
                    case (e.key == "ArrowLeft"): //scrub backward
                        e.preventDefault();
                        sr.querySelector(`[number="${activeIndex}"]`).dispatchEvent(new CustomEvent('scrub-backward'));
                        break;
                    case (e.key == "ArrowUp" && activeIndex > 0): //play previous
                        e.preventDefault();
                        handlePlay(activeIndex - 1);
                        scrollToActive();
                        break;
                    case (e.key == "ArrowRight"): //scrub forward
                        e.preventDefault();
                        sr.querySelector(`[number="${activeIndex}"]`).dispatchEvent(new CustomEvent('scrub-forward'));
                        break;
                    case (e.key == "ArrowDown"): //play next
                        e.preventDefault();
                        handlePlay(activeIndex + 1);
                        scrollToActive();
                        break;
                    case (e.key == "c" || e.key == "C"): //copy
                        e.preventDefault();
                        sr.querySelector(`[copyindex="${activeIndex}"]`).dispatchEvent(new CustomEvent("copy"));
                        break;
                    case ((e.key == "l" || e.key == "L") && !stateFocus): //loop
                        e.preventDefault();
                        controlLoop();
                        break;
                    case (e.key == "v" || e.key == "V"): //toggle visualizer
                        e.preventDefault();
                        toggleVis();
                        break;
                };
            };
        });
    };
};
window.customElements.define("comp-sample-grid", CompSampleGrid);
