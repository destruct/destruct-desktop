class CompIcon extends HTMLElement{
    static observedAttributes = ["img"];

    constructor(){
        super();
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
            <style>
                #icon{
                    background-color: ${this.getAttribute("color")};
                    transition-duration: 0.5s;
                    transition-property: background-color;
                    cursor: pointer;
                    height: ${this.getAttribute("height")};
                    width: ${this.getAttribute("width") != null ? this.getAttribute("width") : this.getAttribute("height")};
                    mask: url(${this.getAttribute("img")});
                    mask-size: cover;
                }
                @media(hover){
                    #icon:hover{
                        background-color: var(--active);
                        transition-duration: 0s;
                    }
                }
            </style>

            <div id="icon"></div>
            <slot><slot>
        `;
    };

    attributeChangedCallback(){
        this.shadowRoot.getElementById("icon").style.mask = `url(${this.getAttribute("img")})`;
    };
};
window.customElements.define("comp-icon", CompIcon);
