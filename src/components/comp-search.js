class CompSearch extends HTMLElement{
    connectedCallback(){
        const sr = this.attachShadow({mode: "closed"});
        sr.innerHTML = `
            <style>
                #sidebar{
                    position: relative;
                    font-size: 14px;
                    height: 100vh;
                    overscroll-behavior: none;
                    background-color: #0c0c0ccc;
                    border-radius: 10px 0 0 10px;
                    border-top: 1.5px solid #222;
                    border-left: 1.5px solid #222;
                    border-bottom: 1.5px solid #222;
                    box-sizing: border-box;
                }
                    #search-wrapper{
                        position: fixed;
                        top: 45px;
                        width: 220.5px;
                        box-sizing: border-box;
                        display: flex;
                        flex-direction: column;
                        gap: 10px;
                        padding: 10px;
                        border-top: 1.5px solid #222;
                        border-bottom: 1.5px solid #222;
                        box-sizing: border-box;
                        background-color: #0000;
                    }
                        #search{
                            caret-color: var(--active);
                            color: var(--active);
                            font-family: abel;
                            font-size: 14px;
                            letter-spacing: 0.09em;
                            text-transform: uppercase;
                            box-sizing: border-box;
                            border: 1.5px solid #222;
                            border-radius: 4px;
                            height: min-content;
                            width: 100%;
                            height: 36px;
                            padding: 0 10px;
                            transition: color 0.5s, border-color 0.5s;
                            background-color: #0000;
                        }
                            #search:focus{
                                outline: 0;
                                border-color: var(--active);
                                transition-duration: 0s;
                            }
                            #search::selection{
                                color: var(--main);
                                background: var(--active);
                            }
                            #search-spacer{
                                height: calc(94px + 45px);
                            }
                        #sort-filters{
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                    #groups{
                        position: absolute;
                        overflow-y: scroll;
                        height: calc(100vh - 142px);
                        top: 139px;
                        width: 100%;
                        box-sizing: border-box;
                        display: flex;
                        flex-direction: column;
                        gap: 5px;
                        padding: 10px;
                        overscroll-behavior: none;
                    }
                        #groups::-webkit-scrollbar{
                            scrollbar-color: #0000;
                            display: none;
                        }
                        .types{
                            display: none;
                            height: min-content;
                            flex-direction: column;
                            gap: 5px;
                            padding-left: 30px;
                        }
                            .tag{
                                cursor: pointer;
                                border-style: solid;
                                border-width: 1.5px;
                                border-color: #0000;
                                border-radius: 4px;
                                height: 25px;
                                display: flex;
                                align-items: center;
                                color: var(--bright);
                                transition-duration: 0.5s;
                                padding-left: 8.5px;
                                box-sizing: bounding-box;
                            }
                                .tag[checked="true"]{
                                    color: var(--active);
                                    transition-duration: 0s;
                                }
                @media(hover){
                    #search:hover{
                        color: var(--active);
                        border-color: var(--active);
                        transition-duration: 0s;
                    }
                    .tag:hover{
                        color: var(--active);
                        transition-duration: 0s;
                    }
                }
            </style>

            <div id="sidebar">

                <div id="search-wrapper">
                    <input id="search" type="text"></input>
                    <div id="sort-filters">
                        <comp-sort-filters></comp-sort-filters>
                    </div>
                </div>

                <div id="groups">
                    <div id="Drums" class="tag group">Drums</div>
                    <div id="Drums-Types" class="types">
                        <div id="Break" class="tag type">Break</div>
                        <div id="Clap" class="tag type">Clap</div>
                        <div id="Crash" class="tag type">Crash</div>
                        <div id="Drum Loop" class="tag type">Drum Loop</div>
                        <div id="Hat" class="tag type">Hat</div>
                        <div id="Kick" class="tag type">Kick</div>
                        <div id="Perc" class="tag type">Perc</div>
                        <div id="Perc Loop" class="tag type">Perc Loop</div>
                        <div id="Ride" class="tag type">Ride</div>
                        <div id="Rim" class="tag type">Rim</div>
                        <div id="Shaker" class="tag type">Shaker</div>
                        <div id="Snap" class="tag type">Snap</div>
                        <div id="Snare" class="tag type">Snare</div>
                    </div>
                    <div id="Synths" class="tag group">Synths</div>
                    <div id="Synths-Types" class="types">
                        <div id="808" class="tag type">808</div>
                        <div id="Bass Loop" class="tag type">Bass Loop</div>
                        <div id="Chord" class="tag type">Chord</div>
                        <div id="Color" class="tag type">Color</div>
                        <div id="Drone" class="tag type">Drone</div>
                        <div id="Fog Horn" class="tag type">Fog Horn</div>
                        <div id="Glitch" class="tag type">Glitch</div>
                        <div id="Growl" class="tag type">Growl</div>
                        <div id="Gun" class="tag type">Gun</div>
                        <div id="Hardcore" class="tag type">Hardcore</div>
                        <div id="Metallic" class="tag type">Metallic</div>
                        <div id="Neuro" class="tag type">Neuro</div>
                        <div id="Pad" class="tag type">Pad</div>
                        <div id="Pluck" class="tag type">Pluck</div>
                        <div id="Reese" class="tag type">Reese</div>
                        <div id="Stab" class="tag type">Stab</div>
                        <div id="Sub" class="tag type">Sub</div>
                        <div id="Sustain" class="tag type">Sustain</div>
                        <div id="Womp" class="tag type">Womp</div>
                    </div>
                    <div id="Vox" class="tag group">Vox</div>
                    <div id="Vox-Types" class="types">
                        <div id="Breath" class="tag type">Breath</div>
                        <div id="Chant" class="tag type">Chant</div>
                    </div>
                    <div id="FX" class="tag group">FX</div>
                    <div id="FX-Types" class="types">
                        <div id="Atmosphere" class="tag type">Atmosphere</div>
                        <div id="Creature" class="tag type">Creature</div>
                        <div id="Faller" class="tag type">Faller</div>
                        <div id="Foley" class="tag type">Foley</div>
                        <div id="Gore" class="tag type">Gore</div>
                        <div id="Impact" class="tag type">Impact</div>
                        <div id="Laser" class="tag type">Laser</div>
                        <div id="Liquid" class="tag type">Liquid</div>
                        <div id="Mech" class="tag type">Mech</div>
                        <div id="Noise" class="tag type">Noise</div>
                        <div id="Riser" class="tag type">Riser</div>
                        <div id="Siren" class="tag type">Siren</div>
                        <div id="Stinger" class="tag type">Stinger</div>
                    </div>
                </div>

            </div>
        `;

        /*---PUSH STATE---*/
        async function pushState(){
            //console.log(`stateGroup: ${stateGroup} | stateType: ${stateType}`);
            stateSearch = sr.getElementById("search").value;
            stateFocus = false;
            window.dispatchEvent(new CustomEvent("push-state", {bubbles: true, composed: true}));
            scrollTo(0, -1000); // IOS Safari resizing breaks scrolling to y-0;
        };
        this.addEventListener("sort-state", pushState);

        /*---INITIALIZE---*/
        function initialize(){
            pushState();
            window.removeEventListener("db-init", initialize);
        }
        window.addEventListener("db-init", initialize);

        /*---SEARCH---*/
        function clearSearch(){
            if(stateGroup != "✶"){
                if(stateType  != "✶"){
                    sr.getElementById(stateType).setAttribute("checked", "false");
                }
                sr.getElementById(stateGroup).setAttribute("checked", "false");
                sr.getElementById(`${stateGroup}-Types`).style.display = "none";
            };
            sr.getElementById("search").value = "";
            stateSearch = "";
            stateGroup = "✶";
            stateType = "✶";
            pushState();
        };
        sr.getElementById("search").addEventListener("focus", () => {stateFocus = true});
        sr.getElementById("search").addEventListener("blur", () => {stateFocus = false});

        /*---Groups---*/
        sr.querySelectorAll(".group").forEach(group => {
            group.addEventListener("pointerdown", () => {
                if(stateGroup != group.id){
                    if(stateGroup != "✶"){
                        sr.getElementById(stateGroup).setAttribute("checked", "false");
                        sr.getElementById(`${stateGroup}-Types`).style.display = "none";
                    };
                    group.setAttribute("checked", "true");
                    sr.getElementById(`${group.id}-Types`).style.display = "flex";
                    stateGroup = group.id;
                }
                else{
                    sr.getElementById(`${stateGroup}-Types`).style.display = "none";
                    group.setAttribute("checked", "false");
                    stateGroup = "✶";
                };
                if(stateType != "✶"){sr.getElementById(stateType).setAttribute("checked", "false")};
                stateType = "✶";
                pushState();
            });
        });

        /*---Types---*/
        sr.querySelectorAll(".type").forEach(type => {
            type.addEventListener("pointerdown", () => {
                if(stateType != type.id){
                    if(stateType != "✶"){sr.getElementById(stateType).setAttribute("checked", "false")};
                    type.setAttribute("checked", "true");
                    stateType = type.id;
                }
                else{
                    type.setAttribute("checked", "false");
                    stateType = "✶";
                };
                pushState();
            });
        });

        /*---RANDOMIZE---*/
        const types = sr.querySelectorAll(".type");

        function randomize(){
            let typeIndex = Math.floor(Math.random() * types.length);
            let type = types[typeIndex];
            if(type.id === stateType){type = types[(typeIndex + 1) % types.length]};
            let group = sr.getElementById(type.parentElement.id.slice(0, type.parentElement.id.length - 6));
            if(stateGroup != group.id){
                if(stateGroup != "✶"){
                    sr.getElementById(stateGroup).setAttribute("checked", "false");
                    sr.getElementById(`${stateGroup}-Types`).style.display = "none";
                };
                group.setAttribute("checked", "true");
                sr.getElementById(`${group.id}-Types`).style.display = "flex";
            };
            if(stateType != "✶"){sr.getElementById(stateType).setAttribute("checked", "false")};
            type.setAttribute("checked", "true");
            stateGroup = group.id;
            stateType = type.id;
            pushState();
        };

        /*---KEYBOARD HANDLING---*/
        let cmd = false;
        window.addEventListener("keydown", e => {
            switch(true){
                case ((e.key == "r" || e.key == "R") && !stateFocus && !cmd): //randomize
                    e.preventDefault();
                    randomize();
                    break;
                case ((e.key == "s" || e.key == "S") && !stateFocus): //toggle search
                    e.preventDefault();
                    sr.getElementById("search").value = "";
                    sr.getElementById("search").focus();
                    break;
                case ((e.key == "x" || e.key == "X") && !stateFocus): //clear query
                    e.preventDefault();
                    clearSearch();
                    break;
                case (e.key == "Enter"): //enter search
                    e.preventDefault();
                    sr.getElementById("search").blur();
                    pushState();
                    break;
                case (e.key == "Escape" && stateFocus): //unfocus search
                    e.preventDefault();
                    sr.getElementById("search").blur();
                    break;
                case (e.key == "Meta"): //hold command
                    cmd = true;
                    break;
            };
        });

        window.addEventListener("keyup", e => {
            if(!stateFocus){
                switch(true){
                    case (e.key == "Meta"): //release command
                        cmd = false;
                        break;
                };
            };
        });

        /*---RESIZE---*/
        let maximized = false;
        async function checkSize(){
            if(!maximized && await appWindow.isMaximized()){
                maximized = true;
                sr.getElementById("search-wrapper").style.top = "-1.5px";
                sr.getElementById("search-spacer").style.height = "64.5px";
            }
            else if(maximized && await appWindow.isMaximized() == false){
                maximized = false;
                sr.getElementById("search-wrapper").style.top = "28px";
                sr.getElementById("search-spacer").style.height = "94px";
            }
        }
        const appWindow = window.__TAURI__.window.appWindow;
        //appWindow.onResized(checkSize);
    };
};
window.customElements.define("comp-search", CompSearch);
