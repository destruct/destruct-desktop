const { readFile, exists, BaseDirectory } = window.__TAURI_PLUGIN_FS__;

class CompWaveform extends HTMLElement{
    connectedCallback(){
        const sr = this.attachShadow({mode: "closed"});
        sr.innerHTML = `
            <style>
                #wrapper{
                    position: relative;
                    width: 100%;
                    height: 100%;
                }
                #player{
                    width: 100%;
                    height: 100%;
                    cursor: col-resize;
                }
                    #new{
                        display: none;
                        position: absolute;
                        top: -2px;
                        left: 100%;
                        translate: -75% 0;
                    }
            </style>

            <div id="wrapper">
                <canvas id="player"></canvas>
                <comp-tag width="60px" color="var(--active)" background="var(--overlay)" id="new" img="./assets/sort-new-active.svg"></comp-tag>
            </div>
        `;

        //if((loadDate - Date.parse(this.getAttribute("date"))) < 86400000 * 3){sr.getElementById("new").style.display = "block"}; // 1 day in ms * 3
        const stringBarData = this.getAttribute("waveform").split("");
        const barData = [];
        for(let i = 0; i < 100; i++){barData.push(parseInt(stringBarData[i], 36) / 36)};
        const thisIndex = this.getAttribute("number");
        const canvas = sr.getElementById("player");
        const ctx = canvas.getContext("2d");
        const file = this.getAttribute("file");
        let activeBuffer = audioContext.createBufferSource()
        let audioData, duration, playing, startTime, pOffset, pointerX, bRect, sidebarOffset;
        let progress = 0;
        let resetProgress = false;
        let resize = false;
        let wavFile;

        function waveformConfig(){
            if(resize){resize = false};
            canvas.width = sr.getElementById("player").offsetWidth * dpr;
            canvas.height = sr.getElementById("player").offsetHeight * dpr;
            bRect = sr.getElementById("player").getBoundingClientRect();
            pOffset = (dpr / canvas.width);
            ctx.lineCap = "round";
            ctx.lineWidth = dpr * 2;
        };
        waveformConfig();

        async function read(){
            wavFile = await readFile(`Destruct/${file}`, { baseDir: BaseDirectory.Audio });
            audioData = await audioContext.decodeAudioData(wavFile.buffer);
            duration = audioData.duration;
        }
        read();

        async function play(){
            if(resetProgress){
                progress = 0;
                resetProgress = false;
            };
            /* sometimes safari decides to suspunded the audioContext. No clue why */
            if(audioContext.state === "suspended"){
                audioContext.resume();
                console.log("audioContext got suspended");
            };
            activeBuffer.disconnect();
            //activeBuffer.buffer = null; // might not need this
            if(duration - (progress * duration) < 0.1){progress = 0};
            activeBuffer = audioContext.createBufferSource();
            activeBuffer.connect(analyser);
            activeBuffer.buffer = audioData;
            activeBuffer.start(0, duration * progress, duration - (duration * progress));
            startTime = audioContext.currentTime - (duration * progress);
            playing = true;
            animate();
        };

        function stop(hardStop){
            activeBuffer.disconnect();
            activeBuffer.buffer = null;
            //if(duration - (progress * duration) < 0.1){progress = 1};
            if(playing){playing = false};
            if(stateLoop && progress == 1 && thisIndex == activeIndex && !hardStop){play()};
        };

        function drawCursor(color, xPos){
            let grd = ctx.createLinearGradient((xPos - 0.1) * canvas.width, 0, xPos * canvas.width, 0);
            grd.addColorStop(1, colorActive.slice(0, -1) + " / 10%)");
            grd.addColorStop(0, "transparent");
            ctx.fillStyle = grd;
            ctx.fillRect(xPos * canvas.width, canvas.height * 0.015, canvas.width * -0.1, canvas.height * 0.97);
            ctx.strokeStyle = color;
            ctx.beginPath();
            ctx.moveTo(xPos * canvas.width - dpr, canvas.height * 0.025);
            ctx.lineTo(xPos * canvas.width - dpr, canvas.height * 0.975);
            ctx.stroke();
        };

        function drawWave(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            if(resize){waveformConfig()};
            if(playing){progress = Math.min((audioContext.currentTime - startTime) / duration, 1)};
            for (let h = 0; h < 100; h++){ // 100 = number of bars
                const bar = (h + 0.5) / 100;
                ctx.strokeStyle = colorBright;
                if(bar < progress - pOffset){ctx.strokeStyle = colorActive};
                if(globalDrag && thisIndex == pointerArea){
                    switch(true){
                        case(pointerX > progress && bar < progress - pOffset): ctx.strokeStyle = colorActive; break;
                        case(pointerX > progress && bar < pointerX - pOffset): ctx.strokeStyle = colorActiveTrans; break;
                        case(pointerX < progress && bar < pointerX - pOffset): ctx.strokeStyle = colorActive; break;
                        case(pointerX < progress && bar < progress - pOffset): ctx.strokeStyle = colorActiveTrans; break;
                    };
                };
                ctx.beginPath();
                ctx.moveTo((h + 0.5) * (canvas.width / 100), (canvas.height * 0.5) - (canvas.height * 0.5 * barData[h]));
                ctx.lineTo((h + 0.5) * (canvas.width / 100), (canvas.height * 0.5) + (canvas.height * 0.5 * barData[h]));
                ctx.stroke();
            };
            if(progress > 0){
                if(globalDrag && thisIndex == pointerArea){drawCursor(pointerX > progress ? colorActive : colorActiveTrans, progress)}
                else{drawCursor(colorActive, progress)};
            };
            if(globalDrag && thisIndex == pointerArea){drawCursor(pointerX < progress ? colorActive : colorActiveTrans, pointerX)};
        };
        drawWave();

        function animate(){
            if(progress == 1 && playing){stop(); return};
            sidebarOffset = stateSidebar ? 222 : 0;
            if(globalDrag){pointerX = Math.min(Math.max(((globalPointerX + sidebarOffset) - bRect.x) / bRect.width, 0), 1)};
            if(playing || globalDrag || resize){
                drawWave();
                requestAnimationFrame(animate);
            };
        };

        /*---RESIZE---*/
        const resizeObserver = new ResizeObserver(() => {resize = true; animate()});
        resizeObserver.observe(sr.getElementById("player"));

        this.addEventListener("handlePlay", () => {
            if(globalDrag){progress = pointerX};
            if(playing && !globalDrag){stop()}
            else{play()};
        });

        this.addEventListener("scrub-forward", () => {
            if(progress + 0.125 < 1){progress += 0.125}
            else{progress = 0};
            if(playing){play()}
            else{drawWave()};
        });

        this.addEventListener("scrub-backward", () => {
            if(progress - 0.125 > 0){progress -= 0.125}
            else{progress = 1};
            if(playing){play()}
            else{drawWave()};
        });

        this.addEventListener("stop", () => {
            stop(true)
        });

        this.addEventListener("reset-progress", () => {resetProgress = true});

        this.addEventListener("drag", animate);

        this.addEventListener("themeChange", drawWave);
    };
};
window.customElements.define("comp-waveform", CompWaveform);
