class CompSpectrum extends HTMLElement{
    connectedCallback(){
        const sr = this.attachShadow({mode: "closed"})
        sr.innerHTML = `<canvas id="canvas" style="width: 100%; height: 100%;"></canvas>`;

        /*---INITIALIZE---*/
        const canvas = sr.getElementById("canvas");
        const gl = canvas.getContext("webgl2");
        const program = gl.createProgram();
        let resize = false;
        gl.drawingBufferColorSpace = "display-p3";
        gl.unpackColorSpace = "srgb";

        analyser.fftSize = 1024;
        let binCount = analyser.fftSize * 0.5 * 0.75;
        const dataArray = new Uint8Array(binCount);
        let bins = [];
        let floatBins = new Float32Array();

        function makeFloatBins(){
            bins = [];
            for(let i = 0; i < binCount; i++){bins.push((dataArray[i] / 255) * 0.8667)};
            floatBins = new Float32Array(bins);
        };

        /*---VERTEX SHADER---*/
        const vertBuffer = gl.createBuffer();
        const points = new Float32Array([-1.0, -1.0, 0.0, 1.0, -1.0, 0.0, -1.0,  1.0, 0.0, -1.0, 1.0, 0.0, 1.0, -1.0, 0.0, 1.0, 1.0, 0.0]);
        gl.bindBuffer(gl.ARRAY_BUFFER, vertBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, points, gl.STATIC_DRAW);
        gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(0);
        const vertexShader = gl.createShader(gl.VERTEX_SHADER);
        const vertexShaderSource = `#version 300 es
            layout(location = 0) in vec3 position;
            out vec2 uv;

            void main(){
                gl_Position = vec4(position.x, position.y, 0.0, 1.0);
                uv = position.xy * 0.5 + 0.5;
            }
        `;
        gl.shaderSource(vertexShader, vertexShaderSource);
        gl.compileShader(vertexShader);
        gl.attachShader(program, vertexShader);

        /*---FRAGMENT SHADER---*/
        const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
        const fragmentShaderSource = `#version 300 es
            precision mediump float;
            out vec4 fragColor;
            in vec2 uv;
            uniform float canvasX;
            uniform float canvasY;
            uniform sampler2D fData;

            void main(){
                float gridSize = canvasY * 0.06;
                float radius = 0.135;
                float smoothing = gridSize * 0.0013;

                float xOffset = mod(gridSize * (canvasX / canvasY), 1.0) * 0.5;
                float yOffset = mod(gridSize, 1.0) * 0.5;
                vec2 coordX = vec2((uv.x * gridSize * (canvasX / canvasY)) - xOffset, 0);
                vec2 coordY = vec2(0, (uv.y * gridSize) - yOffset);
                vec2 coord = coordX + coordY;
                coord = mod(coord, 1.0);
                float dist = distance(coord, vec2(0.5, 0.5));
                dist = smoothstep(radius - smoothing, radius + smoothing, dist);
                dist = 1.0 - dist;

                float fftColor = texture(fData, vec2((1.0 - abs(uv.x - 0.5) * 2.0), 0.0)).r;
                float color = fftColor * (1.0 - abs(uv.y - 0.5));
                color = smoothstep(0.3, 0.45, color);

                vec4 fg = vec4(color + 0.1333, 0.1333 - (color * 0.1333), 0.1333 - (color * 0.1333) + (color * 0.2), 1);
                vec4 bg = vec4(0.0, 0.0, 0.0, 0);
                fragColor = mix(bg, fg, dist);
            }
        `;
        gl.shaderSource(fragmentShader, fragmentShaderSource);
        gl.compileShader(fragmentShader);
        gl.attachShader(program, fragmentShader);

        /*---ANIMATE---*/
        gl.linkProgram(program);
        gl.useProgram(program);

        let canvasX = gl.getUniformLocation(program, 'canvasX');
        let canvasY = gl.getUniformLocation(program, 'canvasY');
        let fftData = gl.getUniformLocation(program, 'fftData');

        let fftTexture;
        function makeTexture(){
            if(!fftTexture){fftTexture = gl.createTexture()};
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, fftTexture);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.R32F, binCount, 1, 0, gl.RED, gl.FLOAT, floatBins, 0);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        };

        function config(){
            resize = false;
            gl.canvas.width = canvas.clientWidth * dpr;
            gl.canvas.height = canvas.clientHeight * dpr;
            gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
            gl.uniform1f(canvasX, gl.canvas.width);
            gl.uniform1f(canvasY, gl.canvas.height);
            makeTexture();
        };
        config();
        const resizeObserver = new ResizeObserver(() => {resize = true});
        resizeObserver.observe(sr.getElementById("canvas"));

        function animate(){
            if(resize){config()};
            analyser.getByteFrequencyData(dataArray);
            makeFloatBins();
            makeTexture();
            gl.uniform1i(fftData, 0);
            gl.drawArrays(gl.TRIANGLES, 0, 6);
            requestAnimationFrame(animate);
        };
        animate();
    };
};
window.customElements.define("comp-spectrum", CompSpectrum);
