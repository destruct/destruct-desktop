class CompMenu extends HTMLElement{
    connectedCallback(){
        const sr = this.attachShadow({mode: "open"});
        sr.innerHTML = `
            <style>
                #wrapper{
                    width: 25px;
                    height: 25px;
                    cursor: pointer;
                }
                #img{
                    width: 100%;
                    width: 100%;
                    stroke: #222;
                    transition-duration: 0.5s;
                    transition-property: stroke;
                }
                @media(hover){
                    #img:hover{
                        stroke: var(--active);
                        transition-duration: 0s;
                    }
                }
            </style>

            <div id="wrapper">
                <svg id="img" stroke-linecap="round" fill="none" stroke-width="1.5" version="1.1" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                    <rect x="4" y="4" width="16" height="16" ry="1"/>
                    <path id="middle" d="m10 4.75v14.504"/>
                    <path d="m4 7h16"/>
                </svg>
            </div>
        `;

        window.addEventListener("toggleSidebar", () => {
            let offset = (stateSidebar) ? 0 : -6;
            sr.getElementById("middle").animate(
                [{transform: `translateX(${offset}px)`}],
                {
                    fill: "forwards",
                    duration: 100,
                    easing: "ease-out"
                }
            );
        });
    }
}
window.customElements.define("comp-menu", CompMenu);
