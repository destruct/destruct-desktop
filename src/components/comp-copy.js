class CompCopy extends HTMLElement{
    connectedCallback(){
        const sr = this.attachShadow({mode: "open"});
        sr.innerHTML = `
            <style>
                #img{
                    width: 100%;
                    height: 100%;
                }
            </style>

            <div style="width: 25px; height: 25px; cursor: pointer;">
                <svg id="img" fill="none" stroke="var(--bright)" stroke-linecap="straight" stroke-linejoin="round" stroke-width="1.5" version="1.1" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                    <path id="top" d="M 12.000513,4.7226562 A 2.2036,2.2036 0 0 0 10.930201,5 L 4.0825436,8.8046875 a 0.68799,0.68799 0 0 0 0,1.2031255 l 6.8359384,3.796875 a 2.2291,2.2291 0 0 0 2.164062,0 l 6.835938,-3.798829 a 0.6875,0.6875 0 0 0 0,-1.2011715 L 13.070826,5 A 2.2036,2.2036 0 0 0 12.000513,4.7226562 Z" fill="var(--bright)" />
                    <path  id="bottom" d="M 11.999823,9.9179688 A 2.2036,2.2036 0 0 0 10.929511,10.195313 L 4.0818549,14 a 0.68799,0.68799 0 0 0 0,1.203125 L 10.917792,19 a 2.2291,2.2291 0 0 0 2.164062,0 l 6.835938,-3.796875 a 0.6875,0.6875 0 0 0 0,-1.203125 L 13.070136,10.195313 A 2.2036,2.2036 0 0 0 11.999823,9.9179688 Z" />
                </svg>
            </div>
        `;

        const path = `${audioPath}/Destruct/${this.getAttribute("file")}`;
        const { invoke } = window.__TAURI__.core;

        function copyFile(){
            invoke("copy", {path: path});
            sr.getElementById("top").animate(
                [
                    {transform: "translateY(0px)"},
                    {transform: "translateY(2px)"},
                    {transform: "translateY(0px)"}
                ],
                {
                    duration: 100,
                    easing: "ease-out"
                }
            );
            sr.getElementById("bottom").animate(
                [
                    {transform: "translateY(0px)"},
                    {transform: "translateY(-2px)"},
                    {transform: "translateY(0px)"}
                ],
                {
                    duration: 100,
                    easing: "ease-out"
                }
            );
        }

        this.addEventListener("copy", copyFile);
        sr.addEventListener("click", copyFile);

        this.addEventListener("active", () => {
            let top = sr.getElementById("top");
            top.style.transitionDuration = "0s";
            top.style.stroke = "var(--active)";
            top.style.fill = "var(--active)";
            let bottom = sr.getElementById("bottom");
            bottom.style.transitionDuration = "0s";
            bottom.style.stroke = "var(--active)";
        });

        this.addEventListener("inactive", () => {
            let top = sr.getElementById("top");
            top.style.transitionDuration = "0.5s";
            top.style.stroke = "var(--bright)";
            top.style.fill = "var(--bright)";
            let bottom = sr.getElementById("bottom");
            bottom.style.transitionDuration = "0.5s";
            bottom.style.stroke = "var(--bright)";
        });
    }
}
window.customElements.define("comp-copy", CompCopy);
