class CompSync extends HTMLElement{
    connectedCallback(){
        const sr = this.attachShadow({mode: "closed"});
        sr.innerHTML = `
            <style>
                #canvas-wrapper{
                    width: 100%;
                    height: 25px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    max-width: 330px;
                    border: 2px solid var(--active);
                    box-sizing: border-box;
                    border-radius: 4px;
                    padding: 0 2px;
                }
                    #canvas{
                        width: 100%;
                        height: 100%;
                    }
            </style>
            <div id="canvas-wrapper">
                <canvas id="canvas"></canvas>
            </div>
        `;

        const canvas = sr.getElementById("canvas");
        const ctx = canvas.getContext("2d");
        let syncProgress = 0;
        let resize = false;
        let barCount = 50;

        function waveformConfig(){
            if(resize){resize = false};
            canvas.width = sr.getElementById("canvas").offsetWidth * dpr * 4;
            canvas.height = sr.getElementById("canvas").offsetHeight * dpr * 4;
            ctx.lineCap = "round";
            ctx.lineWidth = dpr * 8;
        };
        waveformConfig();

        function drawWave(){
            syncProgress += 0.001;
            if(syncProgress > 1){syncProgress = 0};
            if(resize){waveformConfig()};
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.strokeStyle = colorActive;
            for (let i = 0; i < barCount; i++){
                if((i + 1) / barCount < syncProgress){
                    ctx.beginPath();
                    ctx.moveTo((i + 0.5) * (canvas.width / 50), (canvas.height * 0.25));
                    ctx.lineTo((i + 0.5) * (canvas.width / 50), (canvas.height * 0.75));
                    ctx.stroke();
                }
                else if(i / barCount < syncProgress){
                    ctx.strokeStyle = `oklch(61% 0.38 14 / ${(syncProgress * barCount) - i})`;
                    ctx.beginPath();
                    ctx.moveTo((i + 0.5) * (canvas.width / 50), (canvas.height * 0.25));
                    ctx.lineTo((i + 0.5) * (canvas.width / 50), (canvas.height * 0.75));
                    ctx.stroke();
                }
            }
        };

        function animate(){
            drawWave();
            requestAnimationFrame(animate);
        };
        animate()

        /*---RESIZE---*/
        const resizeObserver = new ResizeObserver(() => {resize = true});
        resizeObserver.observe(sr.getElementById("canvas"));
        this.addEventListener("themeChange", drawWave);
    };
};
window.customElements.define("comp-sync", CompSync);
