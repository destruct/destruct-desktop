class CompTag extends HTMLElement{
    static observedAttributes = ["img"];

    constructor(){
        super();
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
            <style>
                #tag-wrapper{
                    border-radius: 4px;
                    backdrop-filter: blur(3px);
                    -webkit-backdrop-filter: blur(3px);
                    transform: translate3d(0, 0, 0); /* safari blur freeze fix */
                }
                    #tag{
                        background-color: ${this.getAttribute("color")};
                        mask: url(${this.getAttribute("img")});
                        width: ${this.getAttribute("width")};
                        cursor: pointer;
                        height: 25px;
                        border-radius: 4px;
                        mask-position: center;
                        mask-size: cover;
                        transition: background-color 0.5s;
                    }
                @media(hover){
                    #tag[hover="true"]:hover{
                        background-color: oklch(61% 0.38 14);
                        transition-duration: 0s;
                    }
                }
            </style>
            <div id="tag-wrapper">
                <div id="tag"></div>
            </div>
        `;

        if(this.getAttribute("background")){
            this.shadowRoot.getElementById("tag-wrapper").style.backgroundColor = this.getAttribute("background");
        };

        if(this.getAttribute("hover") === "true"){
            this.shadowRoot.getElementById("tag").setAttribute("hover", "true");
        };
    };

    attributeChangedCallback(){
        this.shadowRoot.getElementById("tag").style.mask = `url(${this.getAttribute("img")})`;
    };
};
window.customElements.define("comp-tag", CompTag);
