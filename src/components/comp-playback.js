class CompPlayback extends HTMLElement{
    connectedCallback(){
        const sr = this.attachShadow({mode: "open"});
        sr.innerHTML = `
            <style>
                #wrapper{
                    width: 25px;
                    height: 25px;
                    cursor: pointer;
                }
                #img{
                    width: 100%;
                    width: 100%;
                    stroke: #222;
                    transition-duration: 0.5s;
                }
                @media(hover){
                    #img:hover{
                        stroke: var(--active);
                        transition-duration: 0s;
                    }
                }
            </style>

            <div id="wrapper">
                <svg id="img" stroke-linecap="straight" stroke-linejoin="round" fill="none" stroke-width="1.5" version="1.1" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                    <g id="main">
                        <path d="m-21 18h13a3 3 0 0 0 3-3v-1"/>
                        <path d="m-3.0115 6h-12.988a3.0029 3.0029 0 0 0-3.0028 3.0115l0.0028 0.98851"/>
                        <line x1="15" x2="3" y1="12" y2="12"/>
                        <path d="m20.25 21v-18"/>
                        <path d="m-7.2426 1.7574 4.2426 4.2426-4.2426 4.2426"/>
                        <path d="m-16.757 22.243-4.2426-4.2426 4.2426-4.2426"/>
                        <path d="m10.05 7.0503 4.9497 4.9497-4.9497 4.9497"/>
                    </g>
                </svg>
            </div>
        `;

        this.addEventListener("playbackChange", () => {
            let offset = (stateLoop) ? 24 : 0;
            sr.getElementById("main").animate(
                [{transform: `translateX(${offset}px)`}],
                {
                    fill: "forwards",
                    duration: 100,
                    easing: "ease-out"
                }
            );
        });
    }
}
window.customElements.define("comp-playback", CompPlayback);
